class Merchant < ActiveRecord::Base
  require 'elasticsearch/model'

  attr_accessor :specialization

  has_many :openings, :dependent => :destroy
  has_and_belongs_to_many :specializations

  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  Merchant.import # for auto sync model with elastic search

end
