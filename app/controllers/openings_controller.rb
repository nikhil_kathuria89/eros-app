class OpeningsController < ApplicationController
  before_action :set_opening, only: [:show, :edit, :update, :destroy]
  before_action :set_merchant

  def index
    @openings = @merchant.openings
  end

  def show
  end

  def new
    @opening = Opening.new
  end

  def edit
  end

  def create
    @opening = @merchant.openings.new(opening_params)

      if @opening.save
        redirect_to merchant_openings_path(@merchant), notice: 'Opening was successfully created.'
      else
        render :new
      end
  end

  def update
      if @opening.update(opening_params)
         redirect_to merchant_openings_path(@merchant), notice: 'Opening was successfully updated.'
      else
          render :edit
      end
  end

  def destroy
    @opening.destroy
    redirect_to openings_url, notice: 'Opening was successfully destroyed.'
  end

  private

    def set_merchant
      @merchant = Merchant.find(params[:merchant_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_opening
      @opening = Opening.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def opening_params
      params.require(:opening).permit(:merchant_id, :start_time, :endtime, :status)
    end
end
