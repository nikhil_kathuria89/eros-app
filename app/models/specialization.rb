class Specialization < ActiveRecord::Base
  require 'elasticsearch/model'

  has_and_belongs_to_many :merchants

  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  Specialization.import # for auto sync model with elastic search

end
