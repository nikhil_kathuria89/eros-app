json.array!(@merchants) do |merchant|
  json.extract! merchant, :id, :name, :email, :status, :about, :gender, :price, :review_count, :avg_rating
  json.url merchant_url(merchant, format: :json)
end
