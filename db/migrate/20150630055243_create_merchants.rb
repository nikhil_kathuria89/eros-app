class CreateMerchants < ActiveRecord::Migration
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :email
      t.boolean :status
      t.text :about
      t.string :gender
      t.float :price
      t.integer :review_count
      t.float :avg_rating

      t.timestamps null: false
    end
  end
end
