class CreateOpenings < ActiveRecord::Migration
  def change
    create_table :openings do |t|
      t.integer :merchant_id
      t.datetime :start_time
      t.datetime :endtime
      t.string :status

      t.timestamps null: false
    end
  end
end
