json.array!(@openings) do |opening|
  json.extract! opening, :id, :merchant_id, :start_time, :endtime, :status
  json.url opening_url(opening, format: :json)
end
