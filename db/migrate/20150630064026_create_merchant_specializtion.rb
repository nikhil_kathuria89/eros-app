class CreateMerchantSpecializtion < ActiveRecord::Migration
  def change
    create_table :merchants_specializations, :id => false do |t|
      t.integer :merchant_id
      t.integer :specialization_id
    end
  end
end
