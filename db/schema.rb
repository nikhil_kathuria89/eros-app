# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150630064026) do

  create_table "merchants", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "email",        limit: 255
    t.boolean  "status",       limit: 1
    t.text     "about",        limit: 65535
    t.string   "gender",       limit: 255
    t.float    "price",        limit: 24
    t.integer  "review_count", limit: 4
    t.float    "avg_rating",   limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "merchants_specializations", id: false, force: :cascade do |t|
    t.integer "merchant_id",       limit: 4
    t.integer "specialization_id", limit: 4
  end

  create_table "openings", force: :cascade do |t|
    t.integer  "merchant_id", limit: 4
    t.datetime "start_time"
    t.datetime "endtime"
    t.string   "status",      limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "specializations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
