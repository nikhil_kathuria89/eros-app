class MerchantsController < ApplicationController
  before_action :set_merchant, only: [:show, :edit, :update, :destroy]

  def home
     @merchants = Merchant.all
     @specializations = Specialization.all
  end

  def search
    if params[:q].nil?
      @merchants = []
    else
      @specializations = Specialization.search(params[:q])
      @spec_merchants = @specializations.records.map(&:merchants)
      @search_merchants = Merchant.search(params[:q]).records
      @merchants = (@spec_merchants.first.to_a + @search_merchants).uniq
    end
    respond_to do |format|
      format.js
    end
  end

  def index
    @merchants = Merchant.all
  end

  def show
  end

  def new
    @merchant = Merchant.new
  end

  def edit
  end

  def create
    @merchant = Merchant.new(merchant_params)
      if @merchant.save
        @merchant.specializations << Specialization.where(:name => params[:merchant][:specialization])
         redirect_to merchants_path, notice: 'Merchant was successfully created.'
      else
         render :new
      end
  end

  def update
      if @merchant.update(merchant_params)
        @merchant.specializations << Specialization.where(:name => params[:merchant][:specialization])
        redirect_to merchants_path, notice: 'Merchant was successfully updated.'
      else
        render :edit
      end
  end

  def destroy
    @merchant.destroy
    redirect_to merchants_url, notice: 'Merchant was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_merchant
      @merchant = Merchant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def merchant_params
      params.require(:merchant).permit(:name, :email, :status, :about, :gender, :price, :review_count, :avg_rating,:specialization)
    end
end
